resource "aws_s3_bucket" "session-logs" {
  acl           = "private"
  bucket        = "ssm-session-logs-us-east-1-${data.aws_caller_identity.current.account_id}"
  force_destroy = "true"
  lifecycle_rule {
    prefix  = ""
    enabled = true
    expiration {
      days = 1830
    }
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.session-manager.id
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = {
    Name = "ssm-session-logs"
  }
}

resource "aws_s3_bucket" "openvpn-client" {
  acl           = "private"
  bucket        = "openvpn-client-us-east-1-${data.aws_caller_identity.current.account_id}"
  force_destroy = "true"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = {
    Name = "openvpn-client"
  }
}
