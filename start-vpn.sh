#!/bin/bash

while getopts "n" opt; do
  case "$opt" in
    n ) NEW_INSTANCE="true" ;;
  esac
done

getOpenVpnInstance() {
  printf "Searching for existing instance...\n"
  INSTANCE_ID=`aws ec2 describe-instances --filters "Name=tag:Name,Values=openvpn-server" "Name=instance-state-name,Values=pending,running" --query Reservations[*].Instances[*].[InstanceId] --region us-east-1 --output text |head -n 1`
  if [ "$INSTANCE_ID" ]; then
    printf "Found instance $INSTANCE_ID\n"
  else
    printf "No existing OpenVPN instance found.\n"
  fi
}

getLaunchTemplate() {
  printf "Getting launch template...\n"
  LAUNCH_TEMPLATE_ID=`aws ec2 describe-launch-templates --launch-template-names openvpn-server --region us-east-1 |jq -r .LaunchTemplates[].LaunchTemplateId`
  LAUNCH_TEMPLATE_VERSION=`aws ec2 describe-launch-template-versions --launch-template-id $LAUNCH_TEMPLATE_ID --region us-east-1 |jq -r .LaunchTemplateVersions[0].VersionNumber`
  printf "Found launch template $LAUNCH_TEMPLATE_ID version $LAUNCH_TEMPLATE_VERSION\n"
}

startInstance() {
  printf "Launching EC2 instance...\n"
  RANDOM_AZ=`cat /dev/urandom | tr -dc 'a-b' | fold -w ${1:-1} | head -n 1`
  SUBNET_ID=`aws ec2 describe-subnets --filters Name=tag:Name,Values=openvpn-ssm-private-$RANDOM_AZ --region us-east-1 |jq -r .Subnets[].SubnetId`
  INSTANCE_ID=`aws ec2 run-instances --launch-template LaunchTemplateId=$LAUNCH_TEMPLATE_ID,Version=$LAUNCH_TEMPLATE_VERSION --subnet-id $SUBNET_ID --region us-east-1 |jq -r .Instances[].InstanceId`
  printf "EC2 instance $INSTANCE_ID launched.\n"
}

waitForInstanceToBeReadyForSsm() {
  printf "Waiting for instance $INSTANCE_ID to be ready for session manager...\n"
  while true; do
    INSTANCE_STATUS="$(aws ssm describe-instance-information --filters "Key=InstanceIds,Values=$INSTANCE_ID" --region us-east-1 |jq -r .InstanceInformationList[0].PingStatus)"
    if [[ "$INSTANCE_STATUS" == "Online" ]]; then
      printf "Instance $INSTANCE_ID is ready for session manager.\n"
      break
    else
      printf "Instance $INSTANCE_ID not yet ready for session manager. Waiting...\n"
      sleep 5
    fi
  done
}

getOpenVpnClientConfig() {
  printf "Retrieving OpenVPN client config file...\n"
  S3_BUCKET="$(aws s3 ls |cut -d ' ' -f3 |grep -P "^openvpn-client-")"
  while true; do
    S3_LIST_RESPONSE="$(aws s3 ls s3://$S3_BUCKET/${INSTANCE_ID}.ovpn)"
    if [ "$S3_LIST_RESPONSE" ]; then
      aws s3 cp s3://$S3_BUCKET/${INSTANCE_ID}.ovpn ssm.ovpn
      printf "OpenVPN client config file downloaded. Use this file in your OpenVPN client.\n"
      break
    else
      printf "Waiting for instance $INSTANCE_ID to upload OpenVPN client config file to S3...\n"
      sleep 5
    fi
  done
}

startSsmPortForward() {
  aws ssm start-session --target $INSTANCE_ID --document-name AWS-StartPortForwardingSession --parameters '{"portNumber":["1194"],"localPortNumber":["1194"]}' --region us-east-1
}

if [ "$NEW_INSTANCE" != "true" ]; then
  getOpenVpnInstance
fi
if [ ! "$INSTANCE_ID" ]; then
  getLaunchTemplate
  startInstance
fi
waitForInstanceToBeReadyForSsm
getOpenVpnClientConfig
startSsmPortForward
