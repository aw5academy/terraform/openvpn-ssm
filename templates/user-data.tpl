#!/bin/bash

curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh
chmod +x openvpn-install.sh

export AUTO_INSTALL=y
export APPROVE_INSTALL=y
export APPROVE_IP=y
export IPV6_SUPPORT=n
export PORT_CHOICE=1
export PROTOCOL_CHOICE=2
export DNS=1
export COMPRESSION_ENABLED=n
export CUSTOMIZE_ENC=n
export CLIENT=ssm
export PASS=1

./openvpn-install.sh

sed -i -e "/^push \"dhcp-option DNS/d" /etc/openvpn/server.conf
sed -i -e "/^push \"redirect-gateway def1 bypass-dhcp\"$/d" /etc/openvpn/server.conf
echo "push \"route 10.0.0.0 255.0.0.0\"" >> /etc/openvpn/server.conf

systemctl restart openvpn@server.service

INSTANCE_ID="$(curl 169.254.169.254/latest/meta-data/instance-id)"
mv /home/ssm-user/ssm.ovpn /root/ssm.ovpn
sed -i "s,^remote .*,remote 127.0.0.1 1194,g" /root/ssm.ovpn
aws s3 cp /root/ssm.ovpn s3://${openvpn-client-bucket}/$${INSTANCE_ID}.ovpn

shutdown +720
