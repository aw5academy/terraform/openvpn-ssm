{
  "Id":"key-consolepolicy-3",
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"Enable IAM User Permissions",
      "Effect":"Allow",
      "Principal":{
        "AWS":"arn:aws:iam::${aws_account_id}:root"
      },
      "Action":"kms:*",
      "Resource":"*"
    },
    {
      "Sid":"Allow use of the key",
      "Effect":"Allow",
      "Principal":{
        "Service": [
          "logs.us-east-1.amazonaws.com"
        ]
      },
      "Action":[
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:DescribeKey"
      ],
      "Resource":"*"
    }
  ]
}
