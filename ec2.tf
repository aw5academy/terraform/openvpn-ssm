data "aws_ami" "amazon-linux-2" {
 most_recent = true
 filter {
   name   = "name"
   values = ["amzn2-ami-hvm*"]
 }
 owners      = ["amazon"]
}

resource "aws_launch_template" "openvpn-server" {
  name = "openvpn-server"
  iam_instance_profile {
    name = aws_iam_instance_profile.session-manager.name
  }
  image_id = data.aws_ami.amazon-linux-2.id
  instance_initiated_shutdown_behavior = "terminate"
  instance_type = "t3a.micro"
  vpc_security_group_ids = [aws_security_group.openvpn-server.id]
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "openvpn-server"
    }
  }
  user_data = base64encode(templatefile("${path.module}/templates/user-data.tpl",
    {
      openvpn-client-bucket = aws_s3_bucket.openvpn-client.id
    }
  ))
}
