resource "aws_cloudwatch_log_group" "session-logs" {
  kms_key_id        = aws_kms_key.session-manager.arn
  name              = "/aws/ssm/session-logs"
  retention_in_days = "7"
  tags = {
    Name = "/aws/ssm/session-logs"
  }
}
