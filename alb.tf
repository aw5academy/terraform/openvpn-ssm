resource "aws_lb" "private" {
  internal           = true
  load_balancer_type = "application"
  name               = "openvpn-server-alb"
  security_groups    = [aws_security_group.alb.id]
  subnets            = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
  tags = {
    Name = "openvpn-server-alb"
  }
}

resource "aws_lb_listener" "http" {
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "OpenVPN over Session Manager is working"
      status_code  = "200"
    }
  }
  load_balancer_arn = aws_lb.private.arn
  port              = "80"
  protocol          = "HTTP"
}
