# openvpn-ssm
Creates the resources needed for the use of [OpenVPN](https://openvpn.net/) over [AWS Systems Manager Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)

# Prerequisites
Perform these prerequisite steps.

## Session Manager
To be able to use Session Manager from the [AWS CLI](https://aws.amazon.com/cli/) you also need to install the [Session Manager Plugin](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html).

### Linux Install Instructions
```
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/linux_64bit/session-manager-plugin.rpm" -o "session-manager-plugin.rpm"
sudo yum install -y session-manager-plugin.rpm
```

### Ubuntu Install Instructions
```
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

### Install Verification
Verify the install by running:
```
session-manager-plugin
```
You should see the following output:
```
The Session Manager plugin is installed successfully. Use the AWS CLI to start a session.
```

## OpenVPN Client
Install the [OpenVPN client](https://openvpn.net/vpn-client/).

## Terraform CLI
Install the [Terraform CLI](https://www.terraform.io/downloads.html).

# Usage
Follow these steps to use this solution.

## Terraform
* Deploy the stack with:
  ```
  git clone https://gitlab.com/aw5academy/terraform/openvpn-ssm.git
  cd openvpn-ssm
  terraform init
  terraform apply
  ```

## Post Apply Steps
Some things are not in Terraform yet and must be set manually. These are the Session Manager preferences. To set these:
- Login to the AWS console;
- Open the Systems Manager service;
- Click on 'Session Manager' under 'Node Management';
- Click on the 'Preferences' tab;
- Click 'Edit';
- Enable KMS Encryption and point to the `alias/session-manager` key;
- Enable session logging to S3 bucket `ssm-session-logs...` with encryption enabled;
- Enable session logging to CloudWatch log group `/aws/ssm/session-logs` with encryption enabled;
- Save the changes;

## Start VPN
Run the following to start a new VPN session:
```
bash start-vpn.sh
```

## OpenVPN Profile
Import the generated `ssm.ovpn` profile into your OpenVPN client and connect.

## WSL Troubleshooting
If the VPN fails to connect on [Windows Subsystem for Linux](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux) try restarting WSL by running the following from a command prompt:
```
wsl --shutdown
```

Then rerun `bash start-vpn.sh` and try to connect from your OpenVPN client again.

# Cleanup
Cleanup this stack by running the following commands:
```
aws ec2 terminate-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=openvpn-server"  --query Reservations[*].Instances[*].[InstanceId] --region us-east-1 --output text |xargs) --region us-east-1
terraform init
terraform destroy
```
