module "vpc" {
  source   = "git::https://gitlab.com/aw5academy/terraform/modules/vpc.git?ref=master"
  vpc-name = "openvpn-ssm"
}

resource "aws_vpc_endpoint" "ec2messages" {
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints.id]
  service_name        = "com.amazonaws.us-east-1.ec2messages"
  subnet_ids          = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
  tags = {
    Name    = "ec2messages-endpoint"
  }
  vpc_endpoint_type = "Interface"
  vpc_id            = module.vpc.vpc-id
}

resource "aws_vpc_endpoint" "ssmmessages" {
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints.id]
  service_name        = "com.amazonaws.us-east-1.ssmmessages"
  subnet_ids          = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
  tags = {
    Name    = "ssmmessages-endpoint"
  }
  vpc_endpoint_type = "Interface"
  vpc_id            = module.vpc.vpc-id
}

resource "aws_vpc_endpoint" "ssm" {
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints.id]
  service_name        = "com.amazonaws.us-east-1.ssm"
  subnet_ids          = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
  tags = {
    Name    = "ssm-endpoint"
  }
  vpc_endpoint_type = "Interface"
  vpc_id            = module.vpc.vpc-id
}
