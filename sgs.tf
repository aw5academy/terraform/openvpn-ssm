################################################
# VPC Endpoint Security Group
################################################
resource "aws_security_group" "endpoints" {
  description = "Security group for VPC endpoints"
  name        = "endpoints-sg"
  tags = {
    Name    = "endpoints-sg"
  }
  vpc_id = module.vpc.vpc-id
}

resource "aws_security_group_rule" "endpoints-https" {
  cidr_blocks = [
    module.vpc.private-a-subnet-cidr,
    module.vpc.private-b-subnet-cidr
  ]
  description       = "HTTPS access from private subnet"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.endpoints.id
  to_port           = 443
  type              = "ingress"
}

################################################
# EC2 Instance Security Group
################################################
resource "aws_security_group" "openvpn-server" {
  description = "openvpn-server security group"
  name        = "openvpn-server"
  tags = {
    Name = "openvpn-server"
  }
  vpc_id      = module.vpc.vpc-id
}

resource "aws_security_group_rule" "openvpn-server-egress" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allowing full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.openvpn-server.id
  to_port           = 0
  type              = "egress"
}

################################################
# ALB Security Group
################################################
resource "aws_security_group" "alb" {
  description = "OpenVPN ALB security group"
  name        = "openvpn-server-alb"
  tags = {
    Name = "openvpn-server-alb"
  }
  vpc_id      = module.vpc.vpc-id
}

resource "aws_security_group_rule" "alb-inbound" {
  description              = "Allow HTTP traffic from OpenVPN server."
  from_port                = "80"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.alb.id
  source_security_group_id = aws_security_group.openvpn-server.id
  to_port                  = "80"
  type                     = "ingress"
}
