provider "aws" {
  default_tags {
    tags = {
      App = "openvpn-ssm"
    }
  }
  region = "us-east-1"
}

data "aws_caller_identity" "current" {}
